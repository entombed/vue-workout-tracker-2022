Приложение для регистрации физических упражнений

Реализовано на *VueJs 3*
## Tестовый пользователь
```
login: user@workout.local
password: 123456
```

### [Live demo](https://vue-workout-tracker-2022-83b62.firebaseapp.com)

![](./screenshot-vue-workout-tracker-2022.png)