import { createRouter, createWebHistory } from "vue-router";
import { supabase } from '../supabase/init'

const routes = [
  {
    path: '/home',
    name: 'home',
    meta: {
      title: 'Список выполненых упражнений',
      protected: true
    },
    component: () => import('../views/Home.vue'),
  },
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Вход в приложение',
      protected: false
    },
    component: () => import('../views/Login.vue'),
  },
  {
    path: '/register',
    name: 'register',
    meta: {
      title: 'Регистрация',
      protected: false
    },
    component: () => import('../views/Register.vue'),
  },
  {
    path: '/create',
    name: 'create',
    meta: {
      title: 'Создать упражнение',
      protected: true
    },
    component: () => import('../views/Create.vue'),
  },
  {
    path: '/view-workout/:workoutId',
    name: 'view-workout',
    meta: {
      title: 'Подробная информация о упражнении',
      protected: true
    },
    component: () => import('../views/ViewWorkout.vue'),
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/home'
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

// Route guard for auth routes
router.beforeEach((to, from, next) => {
  const isRegitered = supabase.auth.user();
  const isProtected = to.matched.some((item) => item.meta.protected)
  if (isProtected) {
    if (isRegitered) {
      document.title = `${to.meta.title}`;
      next();
      return
    } else {
      next({name: 'login'})
      return
    }
  }
  document.title = `${to.meta.title}`;
  next()
})


export default router;
